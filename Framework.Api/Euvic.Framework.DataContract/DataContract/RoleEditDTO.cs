﻿namespace Euvic.Framework.DataContract.DataContract
{
    public class RoleEditDTO
    {
        public bool IsSystemRole { get; set; }
        public string Name { get; set; }
        public int RoleId { get; set; }
    }
}