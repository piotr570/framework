﻿namespace Euvic.Framework.DataContract.DataContract
{
    public class UserDTO
    {
        public string DomainName { get; set; }
        public string Email { get; set; }
        public bool Enabled { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string Surname { get; set; }
    }
}