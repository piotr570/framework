﻿namespace Euvic.Framework.DataContract.DataContract
{
    public class UserViewDTO
    {
        public string DomainName { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Surname { get; set; }
        public int UserId { get; set; }
    }
}