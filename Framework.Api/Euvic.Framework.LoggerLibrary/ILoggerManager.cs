﻿using System;

namespace Euvic.Framework.LoggerLibrary
{
    public enum ActionType
    {
        Display,
        Login,
        Add,
        Delete,
        Update
    }

    public enum Level
    {
        Service,
        UI
    }

    public enum MessageType
    {
        Information,
        Warning,
        Error
    }

    public interface ILoggerManager
    {
        void LogMessage(MessageType type, string level, string msg, string actionType, int userId, string username, string functionality, Exception exp = null);
    }

    public class Functionality
    {
        public const string UserManagement = "Zarządzanie użytkownikami",
        LoginManagement = "Zarządzanie dostępem",
        RoleManagement = "Zarządzanie rolami",
        PermissionManagement = "Zarządzanie pozwoleniami";
    }
}