﻿using NLog;
using NLog.Config;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Euvic.Framework.LoggerLibrary
{
    public class LoggerManager : ILoggerManager
    {
        private const int MethodNameLength = 60;
        private static readonly object Locker = new object();
        private static readonly object padlock = new object();
        private static Logger _logger;
        private static LoggerManager instance;
        private DateTime date = DateTime.Now;

        public LoggerManager()
        {
            string logFilesConfigDirectory = AppDomain.CurrentDomain.BaseDirectory + "/NLog.config";
            LogManager.Configuration = new XmlLoggingConfiguration(logFilesConfigDirectory);
            LogManager.ThrowExceptions = true;
            _logger = LogManager.GetLogger("databaseLogger");
        }

        public static LoggerManager Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new LoggerManager();
                    }
                    return instance;
                }
            }
        }

        public void Log(MessageType messageType, Level level, string message, string actionType, int userId, string username, string functionality, Exception exp)
        {
            LogMessage(messageType, level.ToString(), "An exception occured", actionType, userId, username, functionality, exp);
        }

        public void Log(MessageType messageType, Level level, string message, string actionType, int userId, string username, string functionality)
        {
            LogMessage(messageType, level.ToString(), message, actionType, userId, username, functionality);
        }

        public void LogMessage(MessageType type, string level, string msg, string actionType, int userId, string username, string functionality, Exception exp = null)
        {
            msg = FormatMessage(type, level, msg, exp);
            var log = CreateLog(type, level, msg, actionType, userId, username, functionality, exp);

            switch (type)
            {
                case MessageType.Information:
                    Info(msg, type, level, actionType, userId, username, functionality);
                    break;

                case MessageType.Warning:

                    Warn(msg, type, level, actionType, userId, username, functionality);
                    break;

                case MessageType.Error:
                    Error(msg, type, level, actionType, userId, username, functionality);
                    break;
            }
        }

        #region private

        private string CenterString(string text, int fixedLength)
        {
            if (string.IsNullOrEmpty(text))
                return string.Format("{0}", string.Format("{{0,{0}}}", fixedLength));

            if (text.Length > fixedLength)
                return text.Substring(0, fixedLength);

            return text.PadLeft(((fixedLength - text.Length) / 2)
                            + text.Length)
                   .PadRight(fixedLength);
        }

        private LogEventInfo CreateLog(MessageType type, string level, string message, string actionType, int userId, string username, string functionality, Exception ex = null)
        {
            LogEventInfo log = new LogEventInfo();
            log.Properties["errorType"] = type.ToString().ToUpper().Substring(0, 3);
            log.Properties["levelMessage"] = level;
            log.Properties["messageType"] = actionType;
            log.Properties["userId"] = userId;
            log.Properties["functionality"] = functionality;
            log.Properties["date"] = date;
            log.Properties["username"] = username;
            return log;
        }

        private string FormatMessage(MessageType messageType, string level, string message, Exception ex = null)
        {
            string date = $"{DateTime.Now} |";
            string type = string.Format("[{0}]", messageType.ToString().ToUpper().Substring(0, 3));
            string exceptionMessage = GetExceptionMessage(ex);
            string levelMessage = string.Format("|{0}|", CenterString(level.ToUpper().Replace('_', '-'), 12));
            message = FormatMultiLineMessage(message, false);

            if (string.IsNullOrEmpty(exceptionMessage))
                return string.Format(" {0} ", message);

            return string.Format(" {0} {1} {2} ", message, Environment.NewLine, exceptionMessage);
        }

        private string FormatMultiLineMessage(string message, bool formatFirstLine)
        {
            if (string.IsNullOrEmpty(message))
                return message;

            StringBuilder sb = new StringBuilder();
            int lineNumber = 0;

            using (StringReader reader = new StringReader(message))
            {
                bool isFirstLine = true;
                string line = reader.ReadLine();

                do
                {
                    ++lineNumber;

                    if (!formatFirstLine && lineNumber == 2)
                        sb.AppendLine();

                    if (isFirstLine)
                    {
                        sb.AppendFormat("{1}{0}", line, formatFirstLine ? "\t" : string.Empty);
                        isFirstLine = false;
                    }
                    else
                    {
                        sb.AppendFormat("{0}\t{1}", Environment.NewLine, line);
                    }
                }
                while ((line = reader.ReadLine()) != null);
            }

            if (lineNumber > 1)
                return string.Format("{2}{1}{0}", Environment.NewLine, sb.ToString(), formatFirstLine ? Environment.NewLine : string.Empty);

            return sb.ToString();
        }

        private string GetExceptionMessage(Exception e)
        {
            if (e == null)
                return null;

            Exception ex = e;

            while (ex.InnerException != null)
                ex = ex.InnerException;

            return FormatMultiLineMessage(ex.ToString(), true);
        }

        private string GetMethodName()
        {
            var stackTrace = new StackTrace();
            var frames = stackTrace.GetFrames();
            if (frames.Length < 5)
            {
                return null;
            }

            var method = frames[4].GetMethod();
            if (method == null)
            {
                return null;
            }

            return string.Join(".", method.DeclaringType.FullName, method.Name);
        }

        #endregion private

        #region protected

        protected virtual void Error(string msg, MessageType type, string level, string actionType, int userId, string username, string functionality)
        {
            lock (Locker)
            {
                LogEventInfo info = new LogEventInfo() { Level = LogLevel.Error };
                info.Properties["message"] = msg;
                info.Properties["type"] = type;
                info.Properties["messageType"] = actionType;
                info.Properties["userId"] = userId;
                info.Properties["functionality"] = functionality;
                info.Properties["date"] = date;
                info.Properties["username"] = username;
                _logger.Error(info);
            }
        }

        protected virtual void Info(string msg, MessageType type, string level, string actionType, int userId, string username, string functionality)
        {
            lock (Locker)
            {
                LogEventInfo info = new LogEventInfo() { Level = LogLevel.Info };
                info.Properties["message"] = msg;
                info.Properties["type"] = type;
                info.Properties["level"] = level;
                info.Properties["messageType"] = actionType;
                info.Properties["userId"] = userId;
                info.Properties["functionality"] = functionality;
                info.Properties["date"] = date;
                info.Properties["username"] = username;
                _logger.Info(info);
            }
        }

        protected virtual void Warn(string msg, MessageType type, string level, string actionType, int userId, string username, string functionality)
        {
            lock (Locker)
            {
                LogEventInfo info = new LogEventInfo() { Level = LogLevel.Warn };
                info.Properties["message"] = msg;
                info.Properties["type"] = type;
                info.Properties["level"] = level;
                info.Properties["messageType"] = actionType;
                info.Properties["userId"] = userId;
                info.Properties["functionality"] = functionality;
                info.Properties["date"] = date;
                info.Properties["username"] = username;
                _logger.Warn(info);
            }
        }

        #endregion protected
    }
}