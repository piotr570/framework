﻿namespace Euvic.Framework.DatabaseService.Constants
{
    public class Permissions
    {
        public const string ShowLogs = "ShowLogs",
       ShowUsers = "ShowUsers",
       ShowRoles = "ShowRoles",
       AddUser = "AddUser",
       EditUser = "EditUser",
       DeleteUser = "DeleteUser",
       EditRole = "EditRole",
       DeleteRole = "DeleteRole",
       AssignRole = "AssignRole",
       AddRole = "AddRole";
    }
}