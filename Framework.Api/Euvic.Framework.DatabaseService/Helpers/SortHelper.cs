﻿using System;
using System.Text;

namespace Euvic.Framework.DatabaseService.Helpers
{
    public class SortHelper
    {
        public string PrepareSorting(string sortBy, string sortDir)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(sortBy);
            sb.Append(' ');
            sb.Append(sortDir.ToUpper());
            String sort = sb.ToString();
            return sort;
        }
    }
}