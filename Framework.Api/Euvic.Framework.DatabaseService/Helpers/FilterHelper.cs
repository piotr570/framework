﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Euvic.Framework.DatabaseService.Helpers
{
    public class FilterHelper
    {
        public string PrepareBooleanFiltering(string filterValue)
        {
            var filter = "Enabled = " + filterValue;
            return filter;
        }

        public string PrepareCheckedFiltering(List<int> permissionIds, bool state)
        {
            string filtr = "";
            if (state)
            {
                for (int i = 0; i < permissionIds.Count; i++)
                {
                    filtr += "PermissionId";
                    filtr += " = ";
                    filtr += permissionIds[i];
                    if (i < permissionIds.Count - 1)
                    {
                        filtr += " Or ";
                    }
                }
            }
            else if (!state)
            {
                for (int i = 0; i < permissionIds.Count; i++)
                {
                    filtr += "PermissionId";
                    filtr += " != ";
                    filtr += permissionIds[i];
                    if (i < permissionIds.Count - 1)
                    {
                        filtr += " And ";
                    }
                }
            }
            return filtr;
        }

        public string PrepareDateFiltering()
        {
            var filter = "Date >= @0 And Date <= @1";
            return filter;
        }

        public string PrepareFiltering(string filterBy)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(filterBy);
            sb.Append(".");
            sb.Append("Contains(@0)");
            String filter = sb.ToString();
            return filter;
        }

        public string PrepareMultipleFiltering(List<string> filterBy)
        {
            string filtr = "";
            for (int i = 0; i < filterBy.Count; i++)
            {
                filtr += filterBy[i];
                filtr += ".";
                filtr += "Contains(@" + i + ")";
                if (i < filterBy.Count - 1)
                {
                    filtr += " And ";
                }
            }
            return filtr;
        }
    }
}