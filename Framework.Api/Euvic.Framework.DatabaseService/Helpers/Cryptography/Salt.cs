﻿using System;
using System.Security.Cryptography;

namespace Euvic.Framework.DatabaseService.Helpers.Cryptography
{
    public static class Salt
    {
        public static string GetSalt()
        {
            byte[] bytes = new byte[128 / 8];
            using (var keyGenerator = RandomNumberGenerator.Create())
            {
                keyGenerator.GetBytes(bytes);
                return BitConverter.ToString(bytes).Replace("-", "").ToLower();
            }
        }
    }
}