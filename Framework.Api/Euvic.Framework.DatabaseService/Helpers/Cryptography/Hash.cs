﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Euvic.Framework.DatabaseService.Helpers.Cryptography
{
    public static class Hash
    {
        public static byte[] GetPasswordHash(string password)
        {
            using (var sha256 = SHA256.Create())
            {
                var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(password));
                var tete = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
                return hashedBytes;
            }
        }
    }
}