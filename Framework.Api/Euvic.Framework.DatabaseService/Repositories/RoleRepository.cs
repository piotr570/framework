﻿using Euvic.Framework.Database.Model;
using Euvic.Framework.DatabaseService.Helpers;
using Euvic.Framework.DatabaseService.Interfaces;
using Euvic.Framework.DataContract.DataContract;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Euvic.Framework.DatabaseService.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        private readonly FilterHelper filterHelper = new FilterHelper();
        private readonly SortHelper sortHelper = new SortHelper();

        public async Task AddPermissionsToRole(int roleId, List<int> permissionIds)
        {
            using (var context = new DatabaseContext())
            {
                foreach (var item in permissionIds)
                {
                    var permissionToAdd = new SecRolePermission
                    {
                        RoleId = roleId,
                        PermissionId = item
                    };

                    context.SecRolePermissions.Add(permissionToAdd);
                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task<bool> AddRole(string name)
        {
            using (var context = new DatabaseContext())
            {
                if (await CheckIfRoleExistsInDBInternal(context, name))
                {
                    return false;
                }

                var roleToAdd = new SecRole
                {
                    Name = name,
                    IsSystemRole = false
                };

                context.SecRoles.Add(roleToAdd);

                await context.SaveChangesAsync();

                return true;
            }
        }

        public async Task<bool> CheckIfRoleExistsInDB(string name)
        {
            using (var context = new DatabaseContext())
            {
                return await CheckIfRoleExistsInDBInternal(context, name);
            }
        }

        public async Task<bool> CheckIfRoleIsAssignedToUser(int roleId)
        {
            using (var context = new DatabaseContext())
            {
                var rolesAssignedToUsers = (from ur in context.SecUserRoles
                                            where ur.RoleId == roleId
                                            select ur);

                return await rolesAssignedToUsers.AnyAsync();
            }
        }

        public async Task<bool> CheckIfRoleNameIsUnique(string name, int roleId)
        {
            using (var context = new DatabaseContext())
            {
                var isUnique = (from r in context.SecRoles
                                where r.Name == name && r.RoleId != roleId
                                select r);
                return await isUnique.AnyAsync();
            }
        }

        public async Task<bool> DeleteRole(int roleId)
        {
            using (var context = new DatabaseContext())
            {
                var role = await (from r in context.SecRoles
                                  where r.RoleId == roleId
                                  select r).FirstOrDefaultAsync();

                if (role == null || role.IsSystemRole)
                    return false;

                var permissionsAssignedToDeleteRole = await (from pr in context.SecRolePermissions
                                                             where pr.RoleId == roleId
                                                             select pr).ToListAsync();

                context.SecRolePermissions.RemoveRange(permissionsAssignedToDeleteRole);
                await context.SaveChangesAsync();

                var userRoles = await (from ur in context.SecUserRoles
                                       where ur.RoleId == roleId
                                       select ur).ToListAsync();

                context.SecUserRoles.RemoveRange(userRoles);

                await context.SaveChangesAsync();

                context.SecRoles.Remove(role);

                await context.SaveChangesAsync();

                return true;
            }
        }

        public async Task<KeyValuePair<int, List<SecRole>>> GetAllNonSystemRoles(int pageSize, int pageNumber, string sortBy, string sortDir, List<string> filterBy, List<string> filterValue)
        {
            using (var context = new DatabaseContext())
            {
                var query = (from r in context.SecRoles
                             where r.IsSystemRole == false
                             select r);

                if (filterBy != null && filterBy.Count != 0)
                {
                    var filter = filterHelper.PrepareMultipleFiltering(filterBy);
                    query = query.Where(filter, filterValue.ToArray());
                }

                var sort = sortHelper.PrepareSorting(sortBy, sortDir);
                query = query.OrderBy(sort);
                var roleCount = await query.CountAsync();
                var roles = await query.Skip(pageNumber * pageSize).Take(pageSize).ToListAsync();

                return new KeyValuePair<int, List<SecRole>>(roleCount, roles);
            }
        }

        public async Task<List<int>> GetPermissionIdsForRole(int roleId)
        {
            using (var context = new DatabaseContext())
            {
                var permissionIds = await (from r in context.SecRolePermissions
                                           where r.RoleId == roleId
                                           select r.PermissionId).ToListAsync();
                return permissionIds;
            }
        }

        public async Task<int> GetRoleIdByName(string name)
        {
            using (var context = new DatabaseContext())
            {
                var query = (from r in context.SecRoles
                             where r.Name == name
                             select r.RoleId).FirstOrDefaultAsync();
                return await query;
            }
        }

        public async Task UpdateRole(RoleEditDTO roleToUpdate, List<int> permissionIds)
        {
            using (var context = new DatabaseContext())
            {
                var role = await (from r in context.SecRoles
                                  where r.RoleId == roleToUpdate.RoleId
                                  select r).SingleOrDefaultAsync();

                role.Name = roleToUpdate.Name;

                await context.SaveChangesAsync();

                var permissionIdsInDB = await (from pr in context.SecRolePermissions
                                               where pr.RoleId == roleToUpdate.RoleId
                                               select pr.PermissionId).ToListAsync();

                foreach (var item in permissionIds)
                {
                    var isExist = await (from pr in context.SecRolePermissions
                                         where pr.PermissionId == item && pr.RoleId == roleToUpdate.RoleId
                                         select pr).AnyAsync();
                    if (!isExist)
                    {
                        var permissionToAdd = new SecRolePermission
                        {
                            RoleId = roleToUpdate.RoleId,
                            PermissionId = item
                        };

                        context.SecRolePermissions.Add(permissionToAdd);
                        await context.SaveChangesAsync();
                    }
                }

                var permissionsToDelete = permissionIdsInDB.Except(permissionIds).ToList();

                foreach (var item in permissionsToDelete)
                {
                    var permission = await (from pr in context.SecRolePermissions
                                            where pr.PermissionId == item && pr.RoleId == roleToUpdate.RoleId
                                            select pr).FirstOrDefaultAsync();
                    context.SecRolePermissions.Remove(permission);
                    await context.SaveChangesAsync();
                }
            }
        }

        private async Task<bool> CheckIfRoleExistsInDBInternal(DatabaseContext context, string name)
        {
            var isExist = (from r in context.SecRoles
                           where r.Name == name
                           select r);

            return await isExist.AnyAsync();
        }
    }
}