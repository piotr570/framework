﻿using Euvic.Framework.Database.Model;
using Euvic.Framework.DatabaseService.Helpers;
using Euvic.Framework.DatabaseService.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Euvic.Framework.DatabaseService.Repositories
{
    public class PermissionRepository : IPermissionRepository
    {
        private FilterHelper filterHelper = new FilterHelper();
        private SortHelper sortHelper = new SortHelper();

        public async Task<KeyValuePair<int, List<SecPermission>>> GetPermissions(int pageSize, int pageNumber, string sortBy, string sortDir, List<string> filterBy, List<string> filterValue, List<int> permissionIds = null)
        {
            using (var context = new DatabaseContext())
            {
                var query = (from p in context.SecPermissions
                             select p);
                if (filterBy != null && filterBy.Count != 0 && filterBy.Contains("isChecked"))
                {
                    var checkedIndex = filterBy.FindIndex(x => x == "isChecked");

                    var state = Convert.ToBoolean(filterValue.ElementAt(checkedIndex));
                    if (permissionIds != null)
                    {
                        var filter = filterHelper.PrepareCheckedFiltering(permissionIds, state);
                        query = query.Where(filter, permissionIds.ToArray());
                    }

                    filterBy.RemoveAll(x => x.Contains("isChecked"));
                    filterValue.RemoveAt(checkedIndex);
                }
                if (filterBy != null && filterBy.Count != 0)
                {
                    var filtr = filterHelper.PrepareMultipleFiltering(filterBy);
                    query = query.Where(filtr, filterValue.ToArray());
                }

                var sort = sortHelper.PrepareSorting(sortBy, sortDir);
                query = query.OrderBy(sort);
                var permissionCount = await query.CountAsync();
                var permissions = await query.Skip(pageNumber * pageSize).Take(pageSize).ToListAsync();

                return new KeyValuePair<int, List<SecPermission>>(permissionCount, permissions);
            }
        }
    }
}