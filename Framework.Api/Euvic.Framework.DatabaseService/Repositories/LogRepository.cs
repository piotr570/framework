﻿using Euvic.Framework.Database.Model;
using Euvic.Framework.DatabaseService.Helpers;
using Euvic.Framework.DatabaseService.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Euvic.Framework.DatabaseService.Repositories
{
    public class LogRepository : ILogRepository
    {
        private FilterHelper filterHelper = new FilterHelper();
        private SortHelper sortHelper = new SortHelper();

        public async Task<KeyValuePair<int, List<Log>>> GetAllLogs(int pageSize, int pageNumber, string sortBy, string sortDir, List<string> filterBy, List<string> filterValue)
        {
            using (var context = new DatabaseContext())
            {
                var query = (from l in context.Logs
                             where l.Level != "Error"
                             select l);

                if (filterBy != null && filterBy.Count != 0 && filterBy.Contains("DateFrom") && filterBy.Contains("DateTo"))
                {
                    int dateFromIndex = filterBy.FindIndex(a => a == "DateFrom");
                    int dateToIndex = filterBy.FindIndex(a => a == "DateTo");
                    var dateFrom = Convert.ToDateTime(filterValue.ElementAt(dateFromIndex));
                    var dateTo = Convert.ToDateTime(filterValue.ElementAt(dateToIndex));
                    var filter = filterHelper.PrepareDateFiltering();
                    query = query.Where(filter, dateFrom, dateTo);
                    filterBy.RemoveAll(x => x.Contains("Date"));
                    filterValue.RemoveAt(dateToIndex);
                    filterValue.RemoveAt(dateFromIndex);
                }

                if (filterBy != null && filterBy.Count != 0 && !filterBy.Contains("Date"))
                {
                    var filtr = filterHelper.PrepareMultipleFiltering(filterBy);
                    query = query.Where(filtr, filterValue.ToArray());
                }

                var sort = sortHelper.PrepareSorting(sortBy, sortDir);
                query = query.OrderBy(sort);
                var logsCount = await query.CountAsync();
                var logs = await query.Skip(pageNumber * pageSize).Take(pageSize).ToListAsync();
                return new KeyValuePair<int, List<Log>>(logsCount, logs);
            }
        }

        public async Task<List<string>> GetDataForActionType()
        {
            using (var context = new DatabaseContext())
            {
                var query = (from l in context.Logs
                             select l.ActionType);
                query = query.Distinct();
                var actionTypeList = await query.ToListAsync();
                actionTypeList.RemoveAll(x => x == null || x == "");
                return actionTypeList;
            }
        }

        public async Task<List<string>> GetDataForFunctionality()
        {
            using (var context = new DatabaseContext())
            {
                var query = (from l in context.Logs
                             select l.Functionality);
                query = query.Distinct();
                var funtionalityList = await query.ToListAsync();
                funtionalityList.RemoveAll(x => x == null || x == "");
                return funtionalityList;
            }
        }

        public async Task<List<string>> GetDataForLevel()
        {
            using (var context = new DatabaseContext())
            {
                var query = (from l in context.Logs
                             select l.Level);
                query = query.Distinct();
                var levelList = await query.ToListAsync();
                levelList.RemoveAll(x => x == null || x == "" || x == "Error");
                return levelList;
            }
        }

        public async Task<List<string>> GetDataForTypes()
        {
            using (var context = new DatabaseContext())
            {
                var query = (from l in context.Logs
                             select l.Type);
                query = query.Distinct();
                var typeList = await query.ToListAsync();
                typeList.RemoveAll(x => x == null || x == "");
                return typeList;
            }
        }
    }
}