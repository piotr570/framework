﻿using Euvic.Framework.Database.Model;
using Euvic.Framework.DatabaseService.Helpers;
using Euvic.Framework.DatabaseService.Helpers.Cryptography;
using Euvic.Framework.DatabaseService.Interfaces;
using Euvic.Framework.DataContract.DataContract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Euvic.Framework.DatabaseService.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly FilterHelper _filterHelper = new FilterHelper();
        private readonly SortHelper _sortHelper = new SortHelper();

        public async Task AddUser(UserDTO userToAdd)
        {
            using (var context = new DatabaseContext())
            {
                var salt = Salt.GetSalt();
                var passHash = Hash.GetPasswordHash(userToAdd.Password + salt);

                context.SecUsers.Add(new SecUser
                {
                    DomainName = userToAdd.DomainName,
                    Email = userToAdd.Email,
                    Surname = userToAdd.Surname,
                    Name = userToAdd.Name,
                    Phone = userToAdd.Phone,
                    Enabled = true,
                    Salt = salt,
                    PasswordHash = passHash
                });

                await context.SaveChangesAsync();
            }
        }

        public async Task AssignRole(int userId, List<int> roleIds)
        {
            using (var context = new DatabaseContext())
            {
                var roleIdsInDB = await (from ur in context.SecUserRoles
                                         where ur.UserId == userId
                                         select ur.RoleId).ToListAsync();

                foreach (var item in roleIds)
                {
                    var isExist = await (from ur in context.SecUserRoles
                                         where ur.RoleId == item && ur.UserId == userId
                                         select ur).AnyAsync();

                    if (!isExist)
                    {
                        var roleToAssign = new SecUserRole
                        {
                            UserId = userId,
                            RoleId = item
                        };
                        context.SecUserRoles.Add(roleToAssign);
                        await context.SaveChangesAsync();
                    }
                }

                var rolesToDelete = roleIdsInDB.Except(roleIds).ToList();

                foreach (var item in rolesToDelete)
                {
                    var role = await (from ur in context.SecUserRoles
                                      where ur.RoleId == item && ur.UserId == userId
                                      select ur).FirstOrDefaultAsync();

                    context.SecUserRoles.Remove(role);
                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task<bool> CheckIfUserExistsInDB(string domainName)
        {
            using (var context = new DatabaseContext())
            {
                var isExist = (from u in context.SecUsers
                               where u.DomainName == domainName
                               select u);

                return await isExist.AnyAsync();
            }
        }

        public async Task<bool> CheckIfUserExistsInDBAndIsEnabled(string domainName)
        {
            using (var context = new DatabaseContext())
            {
                return await context
                    .SecUsers
                    .AnyAsync(q => q.DomainName == domainName && q.Enabled);
            }
        }

        public async Task<bool> CheckIfUserMatchesDataInDb(string userName, string password)
        {
            using (var context = new DatabaseContext())
            {
                var users = await context
                    .SecUsers
                    .Where(q => q.DomainName == userName && q.Enabled)
                    .ToListAsync();

                foreach (var user in users)
                {
                    var passHash = Hash.GetPasswordHash(password + user.Salt);
                    if (user.PasswordHash.SequenceEqual(passHash))
                        return true;
                }
            }
            return false;
        }

        public async Task DeleteUser(int userId)
        {
            using (var context = new DatabaseContext())
            {
                var user = (from u in context.SecUsers
                            where u.UserId == userId
                            select u).SingleOrDefault();

                user.Enabled = false;
                await context.SaveChangesAsync();
            }
        }

        public async Task<KeyValuePair<int, List<SecUser>>> GetAllUsers(int pageSize, int pageNumber, string sortBy, string sortDir, List<string> filterBy, List<string> filterValue)
        {
            using (var context = new DatabaseContext())
            {
                var query = (from u in context.SecUsers
                             select u);

                if (filterBy != null && filterBy.Count != 0 && filterBy.Contains("Enabled"))
                {
                    int enabledIndex = filterBy.FindIndex(a => a == "Enabled");
                    var filter = _filterHelper.PrepareBooleanFiltering(filterValue.ElementAt(enabledIndex));
                    query = query.Where(filter);
                    filterBy.RemoveAll(x => x.Contains("Enabled"));
                    filterValue.RemoveAt(enabledIndex);
                }

                if (filterBy != null && filterBy.Count != 0)
                {
                    var filter = _filterHelper.PrepareMultipleFiltering(filterBy);
                    query = query.Where(filter, filterValue.ToArray());
                }

                var sort = _sortHelper.PrepareSorting(sortBy, sortDir);
                query = query.OrderBy(sort);
                var userCount = await query.CountAsync();
                var users = await query.Skip(pageNumber * pageSize).Take(pageSize).ToListAsync();

                return new KeyValuePair<int, List<SecUser>>(userCount, users);
            }
        }

        public async Task<List<string>> GetPermissionsForUser(int userId)
        {
            using (var context = new DatabaseContext())
            {
                var permissionList = await (from ur in context.SecUserRoles
                                            where ur.UserId == userId
                                            join p in context.SecRolePermissions on ur.RoleId equals p.RoleId
                                            join a in context.SecPermissions on p.PermissionId equals a.PermissionId
                                            select a.Name).ToListAsync();

                return permissionList;
            }
        }

        public async Task<List<int>> GetRoleIdsForUser(int userId)
        {
            using (var context = new DatabaseContext())
            {
                var roleIds = await (from ur in context.SecUserRoles
                                     where ur.UserId == userId
                                     select ur.RoleId).ToListAsync();
                return roleIds;
            }
        }

        public async Task<SecUser> GetUserByDomainName(string domainName)
        {
            using (var context = new DatabaseContext())
            {
                var user = await context.SecUsers.FirstOrDefaultAsync(q => q.DomainName == domainName);

                return user;
            }
        }

        public async Task<SecUser> GetUserById(int id)
        {
            using (var context = new DatabaseContext())
            {
                var user = (from u in context.SecUsers
                            where u.UserId == id
                            select u).FirstOrDefaultAsync();
                return await user;
            }
        }

        public async Task<bool> HasUserPermission(string permissionName, int userId)
        {
            using (var context = new DatabaseContext())
            {
                var hasPermission = await (from ur in context.SecUserRoles
                                           where ur.UserId == userId
                                           join p in context.SecRolePermissions on ur.RoleId equals p.RoleId
                                           join a in context.SecPermissions on p.PermissionId equals a.PermissionId
                                           where a.Name == permissionName
                                           select a).AnyAsync();

                return hasPermission;
            }
        }

        public async Task UpdateUser(UserEditDTO userToUpdate)
        {
            using (var context = new DatabaseContext())
            {
                var user = (from u in context.SecUsers
                            where u.UserId == userToUpdate.UserId
                            select u).SingleOrDefault();
                user.Name = userToUpdate.Name;
                user.Surname = userToUpdate.Surname;
                user.Phone = userToUpdate.Phone;
                user.Email = userToUpdate.Email;
                await context.SaveChangesAsync();
            }
        }
    }
}