﻿using Euvic.Framework.Database.Model;
using Euvic.Framework.DataContract.DataContract;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Euvic.Framework.DatabaseService.Interfaces
{
    public interface IRoleRepository
    {
        Task AddPermissionsToRole(int roleId, List<int> permissionIds);

        Task<bool> AddRole(string name);

        Task<bool> CheckIfRoleExistsInDB(string name);

        Task<bool> CheckIfRoleIsAssignedToUser(int roleId);

        Task<bool> CheckIfRoleNameIsUnique(string name, int roleId);

        Task<bool> DeleteRole(int roleId);

        Task<KeyValuePair<int, List<SecRole>>> GetAllNonSystemRoles(int pageSize, int pageNumber, string sortBy, string sortDir, List<string> filterBy, List<string> filterValue);

        Task<List<int>> GetPermissionIdsForRole(int roleId);

        Task<int> GetRoleIdByName(string name);

        Task UpdateRole(RoleEditDTO roleToUpdate, List<int> permissionIds);
    }
}