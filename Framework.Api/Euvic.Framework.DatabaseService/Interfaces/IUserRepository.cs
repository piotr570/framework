﻿using Euvic.Framework.Database.Model;
using Euvic.Framework.DataContract.DataContract;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Euvic.Framework.DatabaseService.Interfaces
{
    public interface IUserRepository
    {
        Task AddUser(UserDTO userToAdd);

        Task AssignRole(int userId, List<int> roleIds);

        Task<bool> CheckIfUserExistsInDB(string domainName);

        Task<bool> CheckIfUserExistsInDBAndIsEnabled(string domainName);

        Task<bool> CheckIfUserMatchesDataInDb(string userName, string password);

        Task DeleteUser(int userId);

        Task<KeyValuePair<int, List<SecUser>>> GetAllUsers(int pageSize, int pageNumber, string sortBy, string sortDir, List<string> filterBy, List<string> filterValue);

        Task<List<string>> GetPermissionsForUser(int userId);

        Task<List<int>> GetRoleIdsForUser(int userId);

        Task<SecUser> GetUserByDomainName(string domainName);

        Task<SecUser> GetUserById(int id);

        Task<bool> HasUserPermission(string permissionName, int userId);

        Task UpdateUser(UserEditDTO userToUpdate);
    }
}