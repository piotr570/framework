﻿using Euvic.Framework.Database.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Euvic.Framework.DatabaseService.Interfaces
{
    public interface ILogRepository
    {
        Task<KeyValuePair<int, List<Log>>> GetAllLogs(int pageSize, int pageNumber, string sortBy, string sortDir, List<string> filterBy, List<string> filterValue);

        Task<List<string>> GetDataForActionType();

        Task<List<string>> GetDataForFunctionality();

        Task<List<string>> GetDataForLevel();

        Task<List<string>> GetDataForTypes();
    }
}