﻿using Euvic.Framework.Database.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Euvic.Framework.DatabaseService.Interfaces
{
    public interface IPermissionRepository
    {
        Task<KeyValuePair<int, List<SecPermission>>> GetPermissions(int pageSize, int pageNumber, string sortBy, string sortDir, List<string> filterBy, List<string> filterValue, List<int> permissionIds = null);
    }
}