﻿using System.Collections.Generic;

namespace Euvic.Framework.DatabaseService.Models
{
    public class AssignRoleParams
    {
        public List<int> RoleIds { get; set; }
        public int UserId { get; set; }
    }
}