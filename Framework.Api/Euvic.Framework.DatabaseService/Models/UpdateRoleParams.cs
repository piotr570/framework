﻿using Euvic.Framework.DataContract.DataContract;
using System.Collections.Generic;

namespace Euvic.Framework.DatabaseService.Models
{
    public class UpdateRoleParams
    {
        public List<int> PermissionIds { get; set; }
        public RoleEditDTO Role { get; set; }
    }
}