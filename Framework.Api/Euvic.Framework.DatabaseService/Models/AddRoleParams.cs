﻿using System.Collections.Generic;

namespace Euvic.Framework.DatabaseService.Models
{
    public class AddRoleParams
    {
        public string Name { get; set; }
        public List<int> PermissionIds { get; set; }
    }
}