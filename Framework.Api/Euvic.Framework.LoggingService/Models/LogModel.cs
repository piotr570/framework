﻿using Euvic.Framework.LoggerLibrary;

namespace Euvic.Framework.LoggingService.Models
{
    public class LogModel
    {
        public string ActionType { get; set; }
        public string Functionality { get; set; }
        public Level Level { get; set; }
        public string Message { get; set; }
        public MessageType Type { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
    }
}