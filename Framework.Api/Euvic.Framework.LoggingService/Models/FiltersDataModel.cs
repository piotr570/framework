﻿using System.Collections.Generic;

namespace Euvic.Framework.LoggingService.Models
{
    public class FiltersDataModel
    {
        public List<string> ActionTypes { get; set; }
        public List<string> Funtionalities { get; set; }
        public List<string> Levels { get; set; }
        public List<string> Types { get; set; }
    }
}