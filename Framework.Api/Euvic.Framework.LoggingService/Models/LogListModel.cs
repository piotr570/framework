﻿using Euvic.Framework.Database.Model;
using System.Collections.Generic;

namespace Euvic.Framework.LoggingService.Models
{
    public class LogListModel
    {
        public List<Log> Logs { get; set; }
        public int Page { get; set; }
        public int Total { get; set; }
    }
}