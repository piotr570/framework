﻿using Euvic.Framework.Authentication.Attributes;
using Euvic.Framework.DatabaseService.Constants;
using Euvic.Framework.DatabaseService.Interfaces;
using Euvic.Framework.LoggerLibrary;
using Euvic.Framework.LoggingService.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Euvic.Framework.LoggingService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class LoggingController : Controller
    {
        private readonly ILoggerManager _loggerManager;
        private readonly ILogRepository _logRepository;
        private readonly IUserRepository _userRepository;

        public LoggingController(
            ILogRepository logRepository,
            ILoggerManager loggerManager,
            IUserRepository userRepository)
        {
            _logRepository = logRepository;
            _loggerManager = loggerManager;
            _userRepository = userRepository;
        }

        [JwtAuthentication(Permissions.ShowLogs)]
        [HttpGet, Route("GetLogs")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllLogs(
            int pageSize,
            int pageNumber,
            [FromQuery] List<string> filterBy,
            [FromQuery] List<string> filterValue,
            string sortBy = "Username",
            string sortDir = "asc")
        {
            var user = await _userRepository.GetUserByDomainName(User.Identity.Name);
            var username = user.Name + " " + user.Surname;
            try
            {
                var logList = await _logRepository.GetAllLogs(pageSize, pageNumber, sortBy, sortDir, filterBy, filterValue);
                var model = new LogListModel
                {
                    Logs = logList.Value,
                    Total = logList.Key
                };

                _loggerManager.LogMessage(
                    MessageType.Information,
                    Level.Service.ToString(),
                    username + " wyświetlił listę logów",
                    ActionType.Display.ToString(),
                    user.UserId,
                    username,
                    Functionality.RoleManagement,
                    null);

                return Ok(model);
            }
            catch (Exception exp)
            {
                _loggerManager.LogMessage(
                    MessageType.Error,
                    Level.Service.ToString(),
                    exp.Message,
                    ActionType.Display.ToString(),
                    user.UserId,
                    username,
                    Functionality.RoleManagement,
                    exp);

                return StatusCode(500);
            }
        }

        [JwtAuthentication(Permissions.ShowLogs)]
        [HttpGet, Route("GetDataForFilters")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetDataForFilters()
        {
            var typeList = await _logRepository.GetDataForTypes();
            var funtionalityList = await _logRepository.GetDataForFunctionality();
            var levelList = await _logRepository.GetDataForLevel();
            var actionTypeList = await _logRepository.GetDataForActionType();
            var model = new FiltersDataModel
            {
                Types = typeList,
                Funtionalities = funtionalityList,
                Levels = levelList,
                ActionTypes = actionTypeList
            };
            return Ok(model);
        }

        [JwtAuthentication]
        [HttpPost, Route("Log")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult Log(LogModel model)
        {
            _loggerManager.LogMessage(model.Type, model.Level.ToString(), model.Message, model.ActionType, model.UserId, model.Username, model.Functionality);
            return Ok("Log dodano do bazy. " + DateTime.Now);
        }
    }
}