﻿using Euvic.Framework.Authentication.Attributes;
using Euvic.Framework.Database.Model;
using Euvic.Framework.DatabaseService.Constants;
using Euvic.Framework.DatabaseService.Interfaces;
using Euvic.Framework.LoggerLibrary;
using Euvic.Framework.ManagementService.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Euvic.Framework.ManagementService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class PermissionController : Controller
    {
        private readonly ILoggerManager _loggerManager;
        private readonly IPermissionRepository _permissionRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly IUserRepository _userRepository;

        public PermissionController(
            IPermissionRepository permissionRepository,
            IUserRepository userRepository,
            IRoleRepository roleRepository,
            ILoggerManager loggerManager)
        {
            _permissionRepository = permissionRepository;
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _loggerManager = loggerManager;
        }

        [HttpGet, Route("GetPermissions")]
        [JwtAuthentication(Permissions.EditRole)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetPermissions(
            int pageSize,
            int pageNumber,
            [FromQuery] List<string> filterBy,
            [FromQuery] List<string> filterValue,
            string sortBy = "Name",
            string sortDir = "asc",
            int roleId = 0)
        {
            var user = await _userRepository.GetUserByDomainName(User.Identity.Name);
            var username = user.Name + " " + user.Surname;
            try
            {
                string isSortBy = sortBy;
                sortBy = sortBy == "isChecked" ? "Name" : sortBy;
                var permissionAssigned = await _roleRepository.GetPermissionIdsForRole(roleId);
                var permissionList = await _permissionRepository.GetPermissions(pageSize, pageNumber, sortBy, sortDir, filterBy, filterValue, permissionAssigned);

                var list = GenerateListWithChecked(permissionList.Value, permissionAssigned);
                var model = new PermissionModel
                {
                    Permissions = list,
                    Total = permissionList.Key
                };

                if (isSortBy == "isChecked")
                {
                    if (sortDir == "asc")
                    {
                        model.Permissions = model.Permissions.OrderByDescending(x => x.isChecked).ToList();
                    }
                    else
                    {
                        model.Permissions = model.Permissions.OrderBy(x => x.isChecked).ToList();
                    }
                }

                _loggerManager.LogMessage(
                    MessageType.Information,
                    Level.Service.ToString(),
                    username + " otworzył listę uprawnień dla roli o id = " + roleId,
                    ActionType.Display.ToString(),
                    user.UserId,
                    username,
                    Functionality.PermissionManagement);

                return Ok(model);
            }
            catch (Exception exp)
            {
                _loggerManager.LogMessage(
                    MessageType.Error,
                    Level.Service.ToString(),
                    exp.Message.ToString(),
                    ActionType.Display.ToString(),
                    user.UserId,
                    username,
                    Functionality.PermissionManagement,
                    exp);

                return StatusCode(500);
            }
        }

        private List<NewPermissionModel> GenerateListWithChecked(List<SecPermission> permissionList, List<int> permissionAssigned)
        {
            List<NewPermissionModel> newPermissionList = new List<NewPermissionModel>();

            foreach (var item in permissionList)
            {
                var isIn = permissionAssigned.Where(x => x == item.PermissionId).Any();
                if (isIn)
                {
                    var model = new NewPermissionModel
                    {
                        PermissionId = item.PermissionId,
                        Name = item.Name,
                        Description = item.Description,
                        Functionality = item.Functionality,
                        isChecked = true
                    };

                    newPermissionList.Add(model);
                }
                else
                {
                    var model = new NewPermissionModel
                    {
                        PermissionId = item.PermissionId,
                        Name = item.Name,
                        Description = item.Description,
                        Functionality = item.Functionality,
                        isChecked = false
                    };

                    newPermissionList.Add(model);
                }
            }

            return newPermissionList;
        }
    }
}