﻿using Euvic.Framework.Authentication.Attributes;
using Euvic.Framework.Database.Model;
using Euvic.Framework.DatabaseService.Constants;
using Euvic.Framework.DatabaseService.Interfaces;
using Euvic.Framework.DatabaseService.Models;
using Euvic.Framework.DataContract.DataContract;
using Euvic.Framework.LoggerLibrary;
using Euvic.Framework.ManagementService.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Euvic.Framework.ManagementService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class UserController : Controller
    {
        private readonly ILoggerManager _loggerManager;
        private readonly IUserRepository _userRepository;

        public UserController(
            IUserRepository userRepository,
            ILoggerManager loggerManager)
        {
            this._userRepository = userRepository;
            this._loggerManager = loggerManager;
        }

        [JwtAuthentication(Permissions.AddUser)]
        [HttpPut, Route("AddUser")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddUser([FromBody] UserDTO userToAdd)
        {
            var currentUser = await GetCurrentUser();
            var username = currentUser.User.Name + " " + currentUser.User.Surname;
            try
            {
                if (userToAdd != null)
                {
                    if (!await _userRepository.CheckIfUserExistsInDB(userToAdd.DomainName))
                    {
                        await _userRepository.AddUser(userToAdd);

                        _loggerManager.LogMessage(MessageType.Information,
                            Level.Service.ToString(),
                            username + " dodał użytkownika",
                            ActionType.Add.ToString(),
                            currentUser.User.UserId,
                            username,
                            Functionality.UserManagement,
                            null);

                        return Ok();
                    }
                }
            }
            catch (Exception exp)
            {
                _loggerManager.LogMessage(
                    MessageType.Error,
                    Level.Service.ToString(),
                    exp.Message,
                    ActionType.Add.ToString(),
                    currentUser.User.UserId,
                    username,
                    Functionality.UserManagement,
                    exp);
            }
            return StatusCode(500);
        }

        [HttpPut, Route("AssignRole")]
        [JwtAuthentication(Permissions.AssignRole)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AssignRole(AssignRoleParams model)
        {
            var user = await _userRepository.GetUserByDomainName(User.Identity.Name);
            var username = user.Name + " " + user.Surname;
            try
            {
                await _userRepository.AssignRole(model.UserId, model.RoleIds);

                _loggerManager.LogMessage(
                    MessageType.Information,
                    Level.Service.ToString(),
                    username + " przypisał role o Id "
                        + string.Join(",", model.RoleIds.Select(q => q.ToString()).ToArray())
                        + " użytkownikowi o Id " + model.UserId,
                    ActionType.Update.ToString(),
                    user.UserId,
                    username,
                    Functionality.UserManagement,
                    null);

                return Ok();
            }
            catch (Exception exp)
            {
                _loggerManager.LogMessage(
                    MessageType.Error,
                    Level.Service.ToString(),
                    exp.Message,
                    ActionType.Update.ToString(),
                    user.UserId,
                    username,
                    Functionality.UserManagement,
                    exp);

                return StatusCode(500);
            }
        }

        [JwtAuthentication(Permissions.DeleteUser)]
        [HttpPut, Route("DeleteUser")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteUser([FromBody] int userId)
        {
            var currentUser = await GetCurrentUser();
            var username = currentUser.User.Name + " " + currentUser.User.Surname;
            try
            {
                var user = await _userRepository.GetUserById(userId);
                if (user == null)
                    return NotFound("User not found");

                await _userRepository.DeleteUser(userId);

                _loggerManager.LogMessage(
                    MessageType.Information,
                    Level.Service.ToString(),
                    username + " usunął użytkownika o Id " + userId,
                    ActionType.Delete.ToString(),
                    currentUser.User.UserId,
                    username,
                    Functionality.UserManagement, null);

                return Ok();
            }
            catch (Exception exp)
            {
                _loggerManager.LogMessage(
                    MessageType.Error,
                    Level.Service.ToString(),
                    exp.Message,
                    ActionType.Delete.ToString(),
                    currentUser.User.UserId,
                    username,
                    Functionality.UserManagement, exp);

                return StatusCode(500);
            }
        }

        [JwtAuthentication(Permissions.ShowUsers)]
        [HttpGet, Route("GetUsers")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllUsers(
            int pageSize,
            int pageNumber,
            [FromQuery] List<string> filterBy,
            [FromQuery] List<string> filterValue,
            string sortBy = "Surname",
            string sortDir = "asc")
        {
            var currentUser = await GetCurrentUser();
            var username = currentUser.User.Name + " " + currentUser.User.Surname;
            try
            {
                var userList = await _userRepository.GetAllUsers(pageSize, pageNumber, sortBy, sortDir, filterBy, filterValue);
                var model = new UsersModel
                {
                    Users = userList.Value,
                    Total = userList.Key,
                    Page = pageNumber
                };

                _loggerManager.LogMessage(
                    MessageType.Information,
                    Level.Service.ToString(),
                    username + " wyświetlił listę użytkowników",
                    ActionType.Display.ToString(),
                    currentUser.User.UserId,
                    username,
                    Functionality.UserManagement,
                    null);

                return Ok(model);
            }
            catch (Exception exp)
            {
                _loggerManager.LogMessage(
                    MessageType.Error,
                    Level.Service.ToString(),
                    exp.Message,
                    ActionType.Display.ToString(),
                    currentUser.User.UserId,
                    username,
                    Functionality.UserManagement,
                    exp);

                return StatusCode(500);
            }
        }

        [JwtAuthentication]
        [HttpGet, Route("GetCurrentUser")]
        public async Task<CurrentUser> GetCurrentUser()
        {
            var domainName = User.Identity.Name;
            var user = await _userRepository.GetUserByDomainName(domainName);
            var permissionList = await _userRepository.GetPermissionsForUser(user.UserId);

            var model = new CurrentUser
            {
                User = new UserViewDTO()
                {
                    Email = user.Email,
                    Name = user.Name,
                    Phone = user.Phone,
                    Surname = user.Surname,
                    UserId = user.UserId,
                    DomainName = user.DomainName
                },
                Permissions = permissionList
            };

            return model;
        }

        [HttpGet, Route("GetRoleIdsForUser")]
        [JwtAuthentication(Permissions.AssignRole)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetRoleIdsForUser(int userId)
        {
            var roleIdsList = await _userRepository.GetRoleIdsForUser(userId);
            return Ok(roleIdsList);
        }

        [JwtAuthentication]
        [HttpGet, Route("GetUserById/{userId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetUserById(int userId)
        {
            var user = await _userRepository.GetUserById(userId);

            if (user == default(SecUser))
                return NotFound("User not found");

            return Ok(new UserEditDTO()
            {
                Email = user.Email,
                Name = user.Name,
                Phone = user.Phone,
                Surname = user.Surname,
                UserId = user.UserId
            });
        }

        [JwtAuthentication(Permissions.EditUser)]
        [HttpPut, Route("UpdateUser")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateUser([FromBody] UserEditDTO userToUpdate)
        {
            var currentUser = await GetCurrentUser();
            var username = currentUser.User.Name + " " + currentUser.User.Surname;
            try
            {
                var user = await _userRepository.GetUserById(userToUpdate.UserId);
                if (user == null || userToUpdate == null)
                    return NotFound("User not found");

                await _userRepository.UpdateUser(userToUpdate);

                _loggerManager.LogMessage(
                    MessageType.Information,
                    Level.Service.ToString(),
                    username + " edytował użytkownika o Id " + userToUpdate.UserId,
                    ActionType.Update.ToString(),
                    currentUser.User.UserId,
                    username,
                    Functionality.UserManagement,
                    null);

                return Ok();
            }
            catch (Exception exp)
            {
                _loggerManager.LogMessage(
                    MessageType.Error,
                    Level.Service.ToString(),
                    exp.Message,
                    ActionType.Update.ToString(),
                    currentUser.User.UserId,
                    username,
                    Functionality.UserManagement,
                    exp);

                return StatusCode(500);
            }
        }
    }
}