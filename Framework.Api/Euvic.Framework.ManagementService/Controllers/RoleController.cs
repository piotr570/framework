﻿using Euvic.Framework.Authentication.Attributes;
using Euvic.Framework.DatabaseService.Constants;
using Euvic.Framework.DatabaseService.Interfaces;
using Euvic.Framework.DatabaseService.Models;
using Euvic.Framework.LoggerLibrary;
using Euvic.Framework.ManagementService.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Euvic.Framework.ManagementService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class RoleController : Controller
    {
        private readonly ILoggerManager _loggerManager;
        private readonly IRoleRepository _roleRepository;
        private readonly IUserRepository _userRepository;

        public RoleController(
            IRoleRepository roleRepository,
            ILoggerManager loggerManager,
            IUserRepository userRepository)
        {
            _roleRepository = roleRepository;
            _loggerManager = loggerManager;
            _userRepository = userRepository;
        }

        [HttpPost, Route("AddRole")]
        [JwtAuthentication(Permissions.AddRole)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddRole(AddRoleParams model)
        {
            var user = await _userRepository.GetUserByDomainName(User.Identity.Name);
            var username = user.Name + " " + user.Surname;
            try
            {
                if (!string.IsNullOrEmpty(model.Name))
                {
                    var state = await _roleRepository.AddRole(model.Name);
                    if (state)
                    {
                        var roleId = await _roleRepository.GetRoleIdByName(model.Name);
                        if (roleId != 0 && model.PermissionIds.Count != 0)
                        {
                            await _roleRepository.AddPermissionsToRole(roleId, model.PermissionIds);
                        }
                    }
                }

                _loggerManager.LogMessage(
                    MessageType.Information,
                    Level.Service.ToString(),
                    username + " dodał rolę " + model.Name,
                    ActionType.Add.ToString(),
                    user.UserId,
                    username,
                    Functionality.RoleManagement,
                    null);

                return Ok();
            }
            catch (Exception exp)
            {
                _loggerManager.LogMessage(
                    MessageType.Error,
                    Level.Service.ToString(),
                    exp.Message,
                    ActionType.Add.ToString(),
                    user.UserId,
                    username,
                    Functionality.RoleManagement,
                    exp);

                return StatusCode(500);
            }
        }

        [HttpGet, Route("CheckIfRoleExistsInDB/{name}")]
        [JwtAuthentication(Permissions.AddRole)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> CheckIfRoleExistsInDB(string name)
        {
            bool outcome = false;

            if (!string.IsNullOrEmpty(name))
                outcome = await _roleRepository.CheckIfRoleExistsInDB(name);

            return Ok(outcome);
        }

        [HttpGet, Route("CheckIfRoleIsAssignedToUser/{roleId}")]
        [JwtAuthentication(Permissions.DeleteRole)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> CheckIfRoleIsAssignedToUser(int roleId)
        {
            var outcome = await _roleRepository.CheckIfRoleIsAssignedToUser(roleId);
            return Ok(outcome);
        }

        [HttpGet, Route("CheckIfRoleNameIsUnique")]
        [JwtAuthentication(Permissions.EditRole)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> CheckIfRoleNameIsUnique(string name, int roleId)
        {
            var outcome = await _roleRepository.CheckIfRoleNameIsUnique(name, roleId);
            return Ok(outcome);
        }

        [HttpPut, Route("DeleteRole")]
        [JwtAuthentication(Permissions.DeleteRole)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteRole([FromBody] int roleId)
        {
            var user = await _userRepository.GetUserByDomainName(User.Identity.Name);
            var username = user.Name + " " + user.Surname;
            try
            {
                if (await _roleRepository.DeleteRole(roleId))
                {
                    _loggerManager.LogMessage(
                        MessageType.Information,
                        Level.Service.ToString(),
                        username + " usunął rolę o Id " + roleId,
                        ActionType.Delete.ToString(),
                        user.UserId,
                        username,
                        Functionality.RoleManagement,
                        null);
                }

                return Ok();
            }
            catch (Exception exp)
            {
                _loggerManager.LogMessage(
                    MessageType.Error,
                    Level.Service.ToString(),
                    exp.Message,
                    ActionType.Delete.ToString(),
                    user.UserId,
                    username,
                    Functionality.RoleManagement,
                    exp);

                return StatusCode(500);
            }
        }

        [HttpGet, Route("GetRoles")]
        [JwtAuthentication(Permissions.ShowRoles)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllNonSystemRoles(
            int pageSize,
            int pageNumber,
            [FromQuery] List<string> filterBy,
            [FromQuery] List<string> filterValue,
            string sortBy = "Name",
            string sortDir = "asc")
        {
            var user = await _userRepository.GetUserByDomainName(User.Identity.Name);
            var username = user.Name + " " + user.Surname;
            try
            {
                var roleList = await _roleRepository.GetAllNonSystemRoles(pageSize, pageNumber, sortBy, sortDir, filterBy, filterValue);
                var model = new RolesModel
                {
                    Roles = roleList.Value,
                    Total = roleList.Key
                };

                _loggerManager.LogMessage(
                    MessageType.Information,
                    Level.Service.ToString(),
                    username + " otworzył listę ról",
                    ActionType.Display.ToString(),
                    user.UserId,
                    username,
                    Functionality.RoleManagement);

                return Ok(model);
            }
            catch (Exception exp)
            {
                _loggerManager.LogMessage(
                    MessageType.Error,
                    Level.Service.ToString(),
                    exp.Message.ToString(),
                    ActionType.Display.ToString(),
                    user.UserId,
                    username,
                    Functionality.RoleManagement,
                    exp);

                return StatusCode(500);
            }
        }

        [HttpGet, Route("GetPermissionIdsForRole/{roleId}")]
        [JwtAuthentication(Permissions.EditRole)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetPermissionIdsForRole(int roleId)
        {
            var permissionIdsList = await _roleRepository.GetPermissionIdsForRole(roleId);
            return Ok(permissionIdsList);
        }

        [HttpPut, Route("UpdateRole")]
        [JwtAuthentication(Permissions.EditRole)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateRole(UpdateRoleParams model)
        {
            var user = await _userRepository.GetUserByDomainName(User.Identity.Name);
            var username = user.Name + " " + user.Surname;
            try
            {
                await _roleRepository.UpdateRole(model.Role, model.PermissionIds);
                _loggerManager.LogMessage(
                    MessageType.Information,
                    Level.Service.ToString(),
                    username + " edytował rolę o Id " + model.Role.RoleId,
                    ActionType.Update.ToString(),
                    user.UserId,
                    username,
                    Functionality.RoleManagement,
                    null);

                return Ok();
            }
            catch (Exception exp)
            {
                _loggerManager.LogMessage(
                    MessageType.Error,
                    Level.Service.ToString(),
                    exp.Message,
                    ActionType.Update.ToString(),
                    user.UserId,
                    username,
                    Functionality.RoleManagement,
                    exp);

                return StatusCode(500);
            }
        }
    }
}