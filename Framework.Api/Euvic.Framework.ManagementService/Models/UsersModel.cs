﻿using Euvic.Framework.Database.Model;
using System.Collections.Generic;

namespace Euvic.Framework.ManagementService.Models
{
    public class UsersModel
    {
        public int Page { get; set; }
        public int Total { get; set; }
        public List<SecUser> Users { get; set; }
    }
}