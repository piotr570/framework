﻿using Euvic.Framework.Database.Model;

namespace Euvic.Framework.ManagementService.Models
{
    public class NewPermissionModel : SecPermission
    {
        public bool isChecked { get; set; }
    }
}