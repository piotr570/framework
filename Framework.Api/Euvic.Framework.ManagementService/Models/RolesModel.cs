﻿using Euvic.Framework.Database.Model;
using System.Collections.Generic;

namespace Euvic.Framework.ManagementService.Models
{
    public class RolesModel
    {
        public int Page { get; set; }
        public List<SecRole> Roles { get; set; }
        public int Total { get; set; }
    }
}