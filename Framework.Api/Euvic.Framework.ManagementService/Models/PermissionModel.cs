﻿using System.Collections.Generic;

namespace Euvic.Framework.ManagementService.Models
{
    public class PermissionModel
    {
        public int Page { get; set; }
        public List<NewPermissionModel> Permissions { get; set; }
        public int Total { get; set; }
    }
}