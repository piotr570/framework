﻿using Euvic.Framework.DataContract.DataContract;
using System.Collections.Generic;

namespace Euvic.Framework.ManagementService.Models
{
    public class CurrentUser
    {
        public List<string> Permissions { get; set; }
        public UserViewDTO User { get; set; }
    }
}