﻿using Euvic.Framework.Authentication.Models;
using System.Threading.Tasks;

namespace Euvic.Framework.Authentication.Interface
{
    public interface IAuthenticationProvider
    {
        Task<string> LoginAndGetToken(LoginModel model);
    }
}