﻿using System.Security.Claims;

namespace Euvic.Framework.Authentication.Provider.Interface
{
    public interface IDomainUserLoginProvider
    {
        bool ValidateCredentials(string userName, string password, out ClaimsIdentity identity);
    }
}