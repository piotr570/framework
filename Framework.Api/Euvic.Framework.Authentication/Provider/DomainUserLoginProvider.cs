﻿using Euvic.Framework.Authentication.Code;
using Euvic.Framework.Authentication.Provider.Interface;
using Microsoft.Extensions.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Security.Claims;

namespace Euvic.Framework.Authentication.Provider
{
    public class DomainUserLoginProvider : IDomainUserLoginProvider
    {
        private static string _domain;
        private IConfiguration _configuration;

        public DomainUserLoginProvider(IConfiguration configuration)
        {
            _configuration = configuration;
            _domain = _configuration.GetValue<string>(AuthenticationModeConsts.AuthenticationDomainSettingName);
        }

        public bool ValidateCredentials(string userName, string password, out ClaimsIdentity identity)
        {
            using (var pc = new PrincipalContext(ContextType.Domain, _domain))
            {
                bool isValid = pc.ValidateCredentials(userName, password, ContextOptions.Negotiate);
                if (isValid)
                {
                    identity = new ClaimsIdentity();
                    identity.AddClaim(new Claim(ClaimTypes.Name, userName));
                }
                else
                {
                    identity = null;
                }

                return isValid;
            }
        }
    }
}