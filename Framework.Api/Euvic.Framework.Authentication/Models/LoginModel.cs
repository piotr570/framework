﻿using System.ComponentModel.DataAnnotations;

namespace Euvic.Framework.Authentication.Models
{
    public class LoginModel
    {
        [Required]
        public string Password { get; set; }

        [Required]
        public string UserName { get; set; }
    }
}