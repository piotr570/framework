﻿namespace Euvic.Framework.Authentication.Models
{
    public class LoginAccessModel
    {
        public string AccessToken { get; set; }
    }
}