﻿using Euvic.Framework.Authentication.Helpers;
using Euvic.Framework.Authentication.Models;
using Euvic.Framework.Authentication.Provider.Interface;
using Euvic.Framework.DatabaseService.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace Euvic.Framework.Authentication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class AuthenticationController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly AuthenticationModeFactory _factory;
        private readonly IDomainUserLoginProvider _provider;
        private readonly IUserRepository _userRepository;

        public AuthenticationController(
            IDomainUserLoginProvider provider,
            IUserRepository userRepository,
            IConfiguration configuration)
        {
            _provider = provider;
            _userRepository = userRepository;
            _configuration = configuration;
            _factory = new AuthenticationModeFactory(provider, userRepository, configuration);
        }

        [AllowAnonymous]
        [HttpGet, Route("RefreshToken/{accessToken}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult RefreshToken(string accessToken)
        {
            var principal = JwtManager.GetPrincipalFromExpiredToken(accessToken);
            var username = principal.Identity.Name;
            var newJwtToken = JwtManager.GenerateToken(username);

            return Ok(new LoginAccessModel
            {
                AccessToken = newJwtToken
            });
        }

        [AllowAnonymous]
        [HttpPost, Route("Token")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Token([FromBody] LoginModel login)
        {
            var authProvider = _factory.CreateAuthenticationProvider();
            var token = await authProvider.LoginAndGetToken(login);

            if (string.IsNullOrEmpty(token))
                return NotFound("Login credentials invalid");

            return Ok(new LoginAccessModel
            {
                AccessToken = token
            });
        }
    }
}