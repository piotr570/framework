﻿using Euvic.Framework.Authentication.Code;
using Euvic.Framework.Authentication.Interface;
using Euvic.Framework.Authentication.Provider.Interface;
using Euvic.Framework.DatabaseService.Interfaces;
using Microsoft.Extensions.Configuration;

namespace Euvic.Framework.Authentication.Helpers
{
    public class AuthenticationModeFactory
    {
        private readonly IDomainUserLoginProvider _provider;
        private readonly IUserRepository _userRepository;
        private readonly string _mode;

        public AuthenticationModeFactory(
            IDomainUserLoginProvider provider,
            IUserRepository userRepository,
            IConfiguration configuration)
        {
            _provider = provider;
            _userRepository = userRepository;
            _mode = configuration[AuthenticationModeConsts.AuthenticationModeSettingName];
        }

        public IAuthenticationProvider CreateAuthenticationProvider()
        {
            if (_mode == AuthenticationModeConsts.DomainAuthenticationMode)
                return new DomainAuthenticationProvider(_provider, _userRepository);
            else if (_mode == AuthenticationModeConsts.DatabaseAuthenticationMode)
                return new DatabaseAuthenticationProvider(_userRepository);
            else
                return new MultiStageAuthenticationProvider(_provider, _userRepository);
        }
    }
}