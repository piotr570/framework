﻿using Euvic.Framework.Authentication.Helpers;
using Euvic.Framework.DatabaseService.Interfaces;
using Euvic.Framework.DatabaseService.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Unity;

namespace Euvic.Framework.Authentication.Attributes
{
    public class JwtAuthenticationAttribute : Attribute, IAuthorizationFilter
    {
        private readonly IUnityContainer _unityContainer;

        public JwtAuthenticationAttribute(string permissionName)
        {
            this._unityContainer = CreateContainer();
            this.userRepository = _unityContainer.Resolve<UserRepository>();
            this.PermissionName = permissionName;
        }

        public JwtAuthenticationAttribute()
        {
            this._unityContainer = CreateContainer();
            this.userRepository = _unityContainer.Resolve<UserRepository>();
        }

        public bool AllowMultiple => false;
        public string PermissionName { get; set; }
        public string Realm { get; set; }
        public IUserRepository userRepository { get; set; }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var request = context.HttpContext.Request;
            var authorizationToken = request.Headers["Authorization"];

            if (string.IsNullOrEmpty(authorizationToken))
            {
                context.Result = new UnauthorizedResult();
                return;
            }

            var isExpired = JwtManager.CheckIfTokenExpired(authorizationToken);
            if (isExpired)
            {
                context.Result = new UnauthorizedResult();
                return;
            }

            var principal = AuthenticateJwtToken(authorizationToken).Result;

            if (principal == null)
            {
                context.Result = new UnauthorizedResult();
                return;
            }

            var user = userRepository.GetUserByDomainName(principal.Identity.Name).Result;
            if (this.PermissionName != null)
            {
                var hasPermission = userRepository.HasUserPermission(this.PermissionName, user.UserId).Result;
                if (!hasPermission)
                {
                    context.Result = new UnauthorizedResult();
                    return;
                }
            }

            if (!userRepository.CheckIfUserExistsInDBAndIsEnabled(principal.Identity.Name).Result)
            {
                context.Result = new ForbidResult();
                return;
            }

            if (principal == null)
                context.HttpContext.Response.WriteAsync("Invalid token");
            else
                context.HttpContext.User = (ClaimsPrincipal)principal;
        }

        protected Task<IPrincipal> AuthenticateJwtToken(string token)
        {
            string username;

            if (ValidateToken(token, out username))
            {
                // based on username to get more information from database in order to build local identity
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, username)
                    // Add more claims if needed: Roles, ...
                };

                var identity = new ClaimsIdentity(claims, "Jwt");
                IPrincipal user = new ClaimsPrincipal(identity);

                return Task.FromResult(user);
            }

            return Task.FromResult<IPrincipal>(null);
        }

        protected virtual IUnityContainer CreateContainer()
        {
            return new UnityContainer();
        }

        private static bool ValidateToken(string token, out string username)
        {
            username = null;

            var simplePrinciple = JwtManager.GetPrincipal(token);
            var identity = simplePrinciple?.Identity as ClaimsIdentity;

            if (identity == null)
                return false;

            if (!identity.IsAuthenticated)
                return false;

            var usernameClaim = identity.FindFirst(ClaimTypes.Name);
            username = usernameClaim?.Value;

            if (string.IsNullOrEmpty(username))
                return false;

            // More validate to check whether username exists in system

            return true;
        }
    }
}