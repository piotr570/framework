﻿namespace Euvic.Framework.Authentication.Code
{
    public class AuthenticationModeConsts
    {
        public const string AuthenticationModeSettingName = "AuthenticationMode";
        public const string AuthenticationDomainSettingName = "Domain";
        public const string DatabaseAuthenticationMode = "database";
        public const string DomainAuthenticationMode = "domain";
    }
}