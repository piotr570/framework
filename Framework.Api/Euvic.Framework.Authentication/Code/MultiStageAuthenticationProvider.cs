﻿using Euvic.Framework.Authentication.Helpers;
using Euvic.Framework.Authentication.Interface;
using Euvic.Framework.Authentication.Models;
using Euvic.Framework.Authentication.Provider.Interface;
using Euvic.Framework.DatabaseService.Interfaces;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Euvic.Framework.Authentication.Code
{
    public class MultiStageAuthenticationProvider : IAuthenticationProvider
    {
        private readonly IDomainUserLoginProvider _provider;
        private readonly IUserRepository _userRepository;

        public MultiStageAuthenticationProvider(
            IDomainUserLoginProvider provider,
            IUserRepository userRepository)
        {
            _provider = provider;
            _userRepository = userRepository;
        }

        public async Task<string> LoginAndGetToken(LoginModel login)
        {
            ClaimsIdentity identity;
            var token = string.Empty;

            if (_provider.ValidateCredentials(login.UserName, login.Password, out identity))
            {
                if (await _userRepository.CheckIfUserExistsInDBAndIsEnabled(login.UserName))
                {
                    token = JwtManager.GenerateToken(login.UserName);
                }
            }
            return token;
        }
    }
}