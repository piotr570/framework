﻿using Euvic.Framework.Authentication.Helpers;
using Euvic.Framework.Authentication.Interface;
using Euvic.Framework.Authentication.Models;
using Euvic.Framework.DatabaseService.Interfaces;
using System.Threading.Tasks;

namespace Euvic.Framework.Authentication.Code
{
    public class DatabaseAuthenticationProvider : IAuthenticationProvider
    {
        private readonly IUserRepository _userRepository;

        public DatabaseAuthenticationProvider(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<string> LoginAndGetToken(LoginModel login)
        {
            var token = string.Empty;
            if (await _userRepository.CheckIfUserMatchesDataInDb(login.UserName, login.Password))
            {
                token = JwtManager.GenerateToken(login.UserName);
            }
            return token;
        }
    }
}