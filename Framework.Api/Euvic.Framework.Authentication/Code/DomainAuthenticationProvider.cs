﻿using Euvic.Framework.Authentication.Helpers;
using Euvic.Framework.Authentication.Interface;
using Euvic.Framework.Authentication.Models;
using Euvic.Framework.Authentication.Provider.Interface;
using Euvic.Framework.DatabaseService.Interfaces;
using Euvic.Framework.DataContract.DataContract;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Euvic.Framework.Authentication.Code
{
    public class DomainAuthenticationProvider : IAuthenticationProvider
    {
        private readonly IDomainUserLoginProvider _provider;
        private readonly IUserRepository _userRepository;

        public DomainAuthenticationProvider(
            IDomainUserLoginProvider provider,
            IUserRepository userRepository)
        {
            _provider = provider;
            _userRepository = userRepository;
        }

        public Task<string> LoginAndGetToken(LoginModel login)
        {
            ClaimsIdentity identity;
            var token = string.Empty;

            return Task.Run(
                async () =>
                {
                    if (_provider.ValidateCredentials(login.UserName, login.Password, out identity))
                    {
                        token = JwtManager.GenerateToken(login.UserName);
                        if (!_userRepository.CheckIfUserExistsInDB(login.UserName).Result)
                        {
                            await _userRepository.AddUser(new UserDTO()
                            {
                                DomainName = login.UserName,
                                Password = login.Password
                            });
                        }
                    }
                    return token;
                });
        }
    }
}