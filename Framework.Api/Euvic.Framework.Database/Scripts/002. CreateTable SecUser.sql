  CREATE TABLE [dbo].[SecUser]
(
	[UserId] INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[Name] NVARCHAR(50) NULL,
	[Surname] NVARCHAR(50) NULL,
	[Phone] NVARCHAR(11) NULL,
	[Email] NVARCHAR(500) NULL,
	[DomainName] NVARCHAR(50) NULL,
	[Enabled] BIT NOT NULL,
	[PasswordHash] BINARY(32) NULL,
	[Salt] NVARCHAR(50) NULL
)