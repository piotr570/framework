CREATE TABLE [dbo].[SecRole]
(
	[RoleId] INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[Name] NVARCHAR(250) NOT NULL,
	[IsSystemRole] BIT NOT NULL
)


CREATE TABLE [dbo].[SecPermission]
(
	[PermissionId] INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[Name] NVARCHAR(250) NOT NULL,
	[Description] NVARCHAR(1000) NOT NULL,
	[Functionality] NVARCHAR(1000) NOT NULL
)
