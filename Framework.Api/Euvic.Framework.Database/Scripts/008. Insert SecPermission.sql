  INSERT INTO [dbo].[SecPermission] ([Name], [Description], [Functionality])
  VALUES ('ShowLogs', 'Pokaż logi', 'Umożliwia podgląd logów')

  INSERT INTO [dbo].[SecPermission] ([Name], [Description], [Functionality])
  VALUES ('ShowUsers', 'Pokaż użytkowników', 'Umożliwia podgląd użytkowników')

  INSERT INTO [dbo].[SecPermission] ([Name], [Description], [Functionality])
  VALUES ('ShowRoles', 'Pokaż role', 'Umożliwia podgląd ról')

  INSERT INTO [dbo].[SecPermission] ([Name], [Description], [Functionality])
  VALUES ('AddUser', 'Dodaj użytkownika', 'Umożliwia dodanie użytkowników')

  INSERT INTO [dbo].[SecPermission] ([Name], [Description], [Functionality])
  VALUES ('EditUser', 'Edycja użytkownika', 'Umożliwia edycję użytkowników')

  INSERT INTO [dbo].[SecPermission] ([Name], [Description], [Functionality])
  VALUES ('DeleteUser', 'Usuń użytkownika', 'Umożliwia usunięcie użytkowników')

  INSERT INTO [dbo].[SecPermission] ([Name], [Description], [Functionality])
  VALUES ('EditRole', 'Edycja roli', 'Umożliwia edycję ról')

  INSERT INTO [dbo].[SecPermission] ([Name], [Description], [Functionality])
  VALUES ('DeleteRole', 'Usuń rolę', 'Umożliwia usunięcie ról')

  INSERT INTO [dbo].[SecPermission] ([Name], [Description], [Functionality])
  VALUES ('AssignRole', 'Przypisz rolę', 'Umożliwia przypisanie ról')

  INSERT INTO [dbo].[SecPermission] ([Name], [Description], [Functionality])
  VALUES ('AddRole', 'Dodaj rolę', 'Umożliwia dodanie ról')