CREATE TABLE [dbo].[SecUserRole]
(
	[UserRoleId] INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[UserId] INT NOT NULL,
	[RoleId] INT NOT NULL
)

ALTER TABLE [dbo].[SecUserRole] ADD CONSTRAINT FK_SecUserRole_User
FOREIGN KEY (UserId) REFERENCES [dbo].[SecUser] (UserId)

ALTER TABLE [dbo].[SecUserRole] ADD CONSTRAINT FK_SecUserRole_Role
FOREIGN KEY (RoleId) REFERENCES [dbo].[SecRole] (RoleId)