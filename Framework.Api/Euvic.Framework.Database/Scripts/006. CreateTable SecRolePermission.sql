CREATE TABLE [dbo].[SecRolePermission]
(
	[RolePermissionId] INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[RoleId] INT NOT NULL,
	[PermissionId] INT NOT NULL
)

ALTER TABLE [dbo].[SecRolePermission] ADD CONSTRAINT FK_SecRolePermission_Role
FOREIGN KEY (RoleId) REFERENCES [dbo].[SecRole] (RoleId)

ALTER TABLE [dbo].[SecRolePermission] ADD CONSTRAINT FK_SecRolePermission_Permission
FOREIGN KEY (PermissionId) REFERENCES [dbo].[SecPermission] (PermissionId)