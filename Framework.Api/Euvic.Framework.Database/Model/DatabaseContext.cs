﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;

namespace Euvic.Framework.Database.Model
{
    public partial class DatabaseContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();

            optionsBuilder.UseSqlServer(configuration.GetConnectionString("DbConnectionString"));
        }

        public DbSet<SecUser> SecUsers { get; set; }
        public DbSet<SecRole> SecRoles { get; set; }
        public DbSet<SecPermission> SecPermissions { get; set; }
        public DbSet<SecRolePermission> SecRolePermissions { get; set; }
        public DbSet<SecUserRole> SecUserRoles { get; set; }
        public DbSet<Log> Logs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SecUser>()
                .HasMany(q => q.UserRoles)
                .WithOne(q => q.User)
                .HasForeignKey(q => q.UserId);

            modelBuilder.Entity<SecUserRole>()
                .HasOne(q => q.Role)
                .WithMany()
                .HasForeignKey(q => q.RoleId);

            modelBuilder.Entity<SecUserRole>()
                .HasOne(q => q.User)
                .WithMany(q => q.UserRoles)
                .HasForeignKey(q => q.UserId);

            modelBuilder.Entity<SecRolePermission>()
                .HasOne(q => q.Permission)
                .WithMany()
                .HasForeignKey(q => q.PermissionId);

            modelBuilder.Entity<SecRolePermission>()
                .HasOne(q => q.Role)
                .WithMany()
                .HasForeignKey(q => q.RoleId);
        }
    }
}