﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Euvic.Framework.Database.Model
{
    [Table("SecRole")]
    public class SecRole
    {
        [Required]
        public bool IsSystemRole { get; set; }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RoleId { get; set; }
    }
}