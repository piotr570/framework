﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Euvic.Framework.Database.Model
{
    [Table("Logs")]
    public class Log
    {
        [Required]
        [StringLength(50)]
        public string ActionType { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        [StringLength(50)]
        public string Functionality { get; set; }

        [Required]
        [StringLength(50)]
        public string Level { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LogId { get; set; }

        [Required]
        public string Message { get; set; }

        [Required]
        [StringLength(50)]
        public string Type { get; set; }

        [Required]
        public int UserId { get; set; }

        [StringLength(50)]
        public string Username { get; set; }
    }
}