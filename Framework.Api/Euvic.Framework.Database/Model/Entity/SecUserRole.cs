﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Euvic.Framework.Database.Model
{
    [Table("SecUserRole")]
    public class SecUserRole
    {
        public SecRole Role { get; set; }

        [Required]
        public int RoleId { get; set; }

        public SecUser User { get; set; }

        [Required]
        public int UserId { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserRoleId { get; set; }
    }
}