﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Euvic.Framework.Database.Model
{
    [Table("SecUser")]
    public class SecUser
    {
        public string DomainName { get; set; }
        public string Email { get; set; }
        public bool Enabled { get; set; }
        public string Name { get; set; }
        public byte[] PasswordHash { get; set; }
        public string Phone { get; set; }
        public string Salt { get; set; }
        public string Surname { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        public List<SecUserRole> UserRoles { get; set; }
    }
}