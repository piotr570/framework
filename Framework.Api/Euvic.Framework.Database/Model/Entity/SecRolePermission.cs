﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Euvic.Framework.Database.Model
{
    [Table("SecRolePermission")]
    public class SecRolePermission
    {
        public int PermissionId { get; set; }

        public SecPermission Permission { get; set; }

        public int RoleId { get; set; }

        public SecRole Role { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RolePermissionId { get; set; }
    }
}